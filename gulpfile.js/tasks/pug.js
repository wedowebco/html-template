const gulp = require('gulp');
const pug = require('gulp-pug');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const path = require('path');
// const { reload } = require('browser-sync');

const config = require('../config');

function requireUncached(module){
    delete require.cache[require.resolve(module)]
    return require(module)
}

module.exports = {
    pug: () => {
        var rev_manifest = requireUncached(`../.${config.root.dist}/assets` + '/rev-manifest.json');

        return gulp.src([`${config.root.src}/${config.pug.src}`, `!${config.root.src}/${config.pug.exclude}`])
            .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
            .pipe(pug({
                pretty: true,
                locals: {
                    css_main: rev_manifest['app.css'],
                    js_app: rev_manifest['app.js']
                }
            }))
            .pipe(gulp.dest(`${config.root.dist}/${config.pug.dist}`));
    }
};